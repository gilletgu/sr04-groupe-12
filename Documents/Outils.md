# Outils pouvant nous être utiles

## REST
[Plugin Mozilla pour tester nos requêtes  REST vers les clouds](https://addons.mozilla.org/fr/firefox/addon/rested/)  
[Plugin chrome pour tester nos requêtes  REST vers les clouds](https://chrome.google.com/webstore/detail/rested/eelcnbccaccipfolokglfhhmapdchbfg)  
