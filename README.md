# SR04 Groupe 12

Bimont Candice
Gillet Guillaume
Chin Juliette
Giramelli Manon

## Backend de la plateforme de gestion multi cloud

# Mise en place des Environements
Vous devez avoir un environement avec nodejs et npm instaler
* installer les dependances: 
```
npm install
npm install -g vue
npm install -g @vue/cli
npm i
```
 
## Environement de dévelopements
* Lancer le frontend:
```
npm run serve
```

## Environemment de production avec nginx
affin de faciliter le deploiment nous vous forunissons un fichier ngnix adapter : Replicut.com
Placer les fichiers de l'aplication dans /var/www/myapp/
puis dans /var/www/myapp/:
* Modifier les fichiers ./src/App.vue, ./src/components/UpField.vue et ./src/components/DownField.vue pour adapter la constante ip avec votre ip.
```
npm run build
sudo systemctl restart nginx
```
